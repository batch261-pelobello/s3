package com.zuitt.example;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int num= 0;
        System.out.println("Input an integer whose factorial will be computed");

        try{
            num = in.nextInt();
        } catch (Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();
        }


        if( num <= 0){
            System.out.println("0 or negative numbers are not factorial numbers");
        } else {
            int answer = 1;
            for(int counter = 1 ; counter<=num ; counter++){
                answer=answer*counter;

            }
            System.out.println("The factorial of "+num + " is " + answer);
        }

 /*       Scanner in = new Scanner(System.in);
        int num = 0;
        System.out.println("Input an integer whose factorial will be computed");
        try {
            num = in.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid input");

        }

        if (num <= 0) {
            System.out.println("0 or negative numbers are not factorial numbers");

        } else{
            int answer = 1;
            int counter = 1;
                while (counter <= num) {
                    answer *= counter;
                    counter++;
                }

        System.out.println("The factorial of "+num + " is " + answer);
        }
*/








    }
}
