package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {
    public static void  main(String[] args){
        //Exceptions
            //a problem that arises during the execution of a program
            //it disrupts the normal flow of the program and terminates it abnormally

        //Exception Handling
            //refers to managing and catching run-time errors in order to safely run our code.
        // Errors encountered in Java
            //Compile-time
            //Run-time

        Scanner input = new Scanner(System.in);
        int num = 0;
        System.out.println("Please enter a number:");
        try{//try to do/execute the statement
            num = input.nextInt();
        }
        catch(Exception e) {
            System.out.println("Invalid input");
            e.printStackTrace();
        }
        //Optional block
        finally {
            //Mostly use to terminate working process apps
            System.out.println("You have entered: " + num);
        }

        //num = input.nextInt();
        System.out.println("Hello World");

    }
}

