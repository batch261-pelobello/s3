package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class RepetitionControl {
    public static void main(String[] args){
       //Loops
            //Are Control Structures that allow code blocks to be executed multiple times

        //While Loop

        int x = 0;
        while (x < 10){
            System.out.println("Loop Number: " + x);
            x++;//iteration;  When to stop
        }

        //Do-While Loop
            // Similar to while loops
            // However, do-while loops will execute atleast once - while loop may not execute at all
        int y = 10;

        do{
            System.out.println("Countdown: " + y);
            y--;//iteration;
        } while(y>10);

        //For Loop
        //Syntax:
        /* for(initialValue; condition ; iteration){
                //code Block
            }*/

        for(int i = 0 ; i < 10 ; i++){
            System.out.println("Current count: " + i);
        }

        //For Loop with Arrays
        int[] intArray = {100, 200, 300, 400, 500};

        for(int i = 0; i < intArray.length; i++){
            System.out.println(i);
            System.out.println(intArray[i]);
        }

        //For-each loop with Array
            //Syntax:
            /* for(dataType itemName : arrayName){
                    //Code block
                }
             */

        String[] nameArray= {"John", "Paul", "George", "Ringo"};
        for(String name : nameArray){
            System.out.println(name);
        }

        System.out.println("----------------------------------");
        //Nested for Loops
        String[][] classroom = new String[3][3];
        //First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third Row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        //outer loop will loop through the rows
        for(int row = 0 ; row < 3 ; row++){
            //inner loop will loop through the columns of each row
            for(int col = 0; col < 3 ; col ++){
                System.out.println("Classroom["+ row + "][" + col + "]" + classroom[row][col]);
            }
        }

        //for-each with multidimensional array
        //access each row
        for(String[] row: classroom){
            //accessing each column (actual element)
            for(String column: row){
                System.out.println(column);
            }
        }

        //for-each with ArrayList
        /* Syntax:
            arrow function/lambda Operator use to separate the
            arrayListName.forEach(consumer -> code-block)
         */

        ArrayList<Integer> numbers = new ArrayList<>();

        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);

        System.out.println("Arraylist: " + numbers);
        // "->" This is called lambda Operator in Java which is used to separate the parameter and implementation
        //-> ~ single arrow for Java ; => for JS
        numbers.forEach(number -> System.out.println("Arraylist: " + number));

        //forEach with HashMaps
        /*
            Syntax:
                    hashMapName.forEach((key,value) -> code-block)
        */

        HashMap<String, Integer> grades = new HashMap<>(){
            {
                put("English", 90);
                put("Math", 95);
                put("Science", 97);
                put("History", 94);
            }

        };
         grades.forEach(((subject, grade) -> System.out.println(subject + ": " + grade)));
    }
}
